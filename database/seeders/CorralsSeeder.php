<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CorralsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('corrals')->insert([
            [
                'id' => 1,
                'name' => 'Загон 1'
            ],
            [
                'id' => 2,
                'name' => 'Загон 2'
            ],
            [
                'id' => 3,
                'name' => 'Загон 3'
            ],
            [
                'id' => 4,
                'name' => 'Загон 4'
            ]
        ]);
    }
}
