<?php

namespace App\Services\CorralService;

use JetBrains\PhpStorm\Pure;

class Client
{
    /**
     * Get instance of refresher.
     *
     * @return CorralRefresher
     */
    public static function refresher(): CorralRefresher
    {
        return new CorralRefresher;
    }

    /**
     * Get instance of representer.
     *
     * @return CorralRepresenter
     */
    public static function representer(): CorralRepresenter
    {
        return new CorralRepresenter;
    }

    /**
     * Get instance of command manager.
     *
     * @return CorralCommandsManager
     */
    #[Pure]
    public static function commandsManager(): CorralCommandsManager
    {
        return new CorralCommandsManager;
    }
}