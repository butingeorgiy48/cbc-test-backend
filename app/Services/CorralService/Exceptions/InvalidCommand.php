<?php

namespace App\Services\CorralService\Exceptions;

use Exception;

class InvalidCommand extends Exception
{
    public int $status = 422;

    public string $defaultMessage = 'Неизвестная команда.';
}