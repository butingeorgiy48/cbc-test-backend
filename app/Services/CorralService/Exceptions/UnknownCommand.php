<?php

namespace App\Services\CorralService\Exceptions;

use Exception;

class UnknownCommand extends Exception
{
    public string $defaultMessage = 'Unknown command.';
}