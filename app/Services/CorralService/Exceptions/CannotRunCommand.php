<?php

namespace App\Services\CorralService\Exceptions;

use Exception;

class CannotRunCommand extends Exception
{
    public string $defaultMessage = 'Cannot run command.';
}