<?php

namespace App\Services\CorralService;

use App\Models\Corral;
use App\Models\Sheep;
use Illuminate\Database\Eloquent\Collection;

class CorralRefresher
{
    /**
     * Collection of corrals.
     *
     * @var Collection<Sheep>
     */
    protected Collection $corrals;

    /**
     * Define is amount of sheep changed.
     *
     * @var bool
     */
    protected bool $hasChanges = false;

    /**
     * CorralRefresher constructor.
     */
    public function __construct()
    {
        $this->corrals = Corral::with('sheep')->get();
    }

    /**
     * Refresh sheep in corrals and return updated data.
     *
     * @return void
     */
    public function refresh(): void
    {
        $forRefreshing = $this->resolveCorralsForRefreshing();

        if ($forRefreshing->count() > 0) {
            $this->generateRandomSheep($forRefreshing);

            $this->hasChanges = true;
        }
    }

    /**
     * Define is amount of sheep changed.
     *
     * @return bool
     */
    public function hasChanges(): bool
    {
        return $this->hasChanges;
    }

    /**
     * Get corrals for refreshing.
     *
     * @return Collection
     */
    protected function resolveCorralsForRefreshing(): Collection
    {
        return $this->corrals->filter(function (Corral $item) {
            return $item->getAmountOfSheep() > 1;
        });
    }

    /**
     * Generate random amount of sheep.
     *
     * @param Collection $corrals
     *
     * @return void
     */
    protected function generateRandomSheep(Collection $corrals): void
    {
        $corrals->each(function (Corral $item) {
            $sheep = Sheep::generateMany(mt_rand(1, 3));

            $item->sheep()->saveMany($sheep);
        });
    }
}