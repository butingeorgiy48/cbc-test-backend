<?php

namespace App\Services\CorralService;

use App\Models\Corral;
use App\Models\Sheep;
use Illuminate\Database\Eloquent\Collection;

class CorralRepresenter
{
    /**
     * Collection of corrals.
     *
     * @var Collection<Sheep>
     */
    protected Collection $corrals;

    /**
     * Representer constructor.
     */
    public function __construct()
    {
        $this->corrals = Corral::with('sheep')->get();
    }

    /**
     * Represent sheep.
     *
     * @return array
     */
    public function represent(): array
    {
        $response = [];

        $this->corrals->each(function (Corral $item) use (&$response) {
            $response[] = [
                'corral' => [
                    'id' => $item->id,
                    'name' => $item->name
                ],
                'sheep' => $item->sheep->makeHidden('corral_id')
            ];
        });

        return $response;
    }
}