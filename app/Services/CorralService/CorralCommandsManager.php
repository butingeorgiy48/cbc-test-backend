<?php

namespace App\Services\CorralService;

use App\Models\Sheep;
use App\Services\CorralService\Exceptions\CannotRunCommand;
use App\Services\CorralService\Exceptions\InvalidCommand;
use App\Services\CorralService\Exceptions\UnknownCommand;
use Exception;

class CorralCommandsManager
{
    /**
     * Command name.
     *
     * @var string
     */
    protected string $command;

    /**
     * Command options.
     *
     * @var array
     */
    protected array $options;

    /**
     * Resolve command.
     *
     * @param string|null $command
     *
     * @return $this
     *
     * @throws InvalidCommand
     */
    public function resolve(?string $command): static
    {
        if (preg_match('/овечка(\d+)\sпереместить\sзагон(\d+)/iu', $command, $matches)) {
            $this->command = 'move';
            $this->options = [
                'sheep_id' => (int) $matches[1],
                'corral_id' => (int) $matches[2]
            ];
        } elseif (preg_match('/убить\sовечка(\d+)/iu', $command, $matches)) {
            $this->command = 'kill';
            $this->options = [
                'sheep_id' => (int) $matches[1]
            ];
        } else {
            throw new InvalidCommand;
        }

        return $this;
    }

    /**
     * Get option.
     *
     * @param string $key
     *
     * @return mixed
     */
    protected function getOption(string $key): mixed
    {
        return $this->options[$key] ?? null;
    }

    /**
     * Run resolved command.
     *
     * @return void
     *
     * @throws CannotRunCommand
     * @throws UnknownCommand
     * @throws Exception
     */
    public function run(): void
    {
        if (!isset($this->command)) {
            throw new CannotRunCommand('Command type has not been resolved.');
        }

        if (!isset($this->options)) {
            throw new CannotRunCommand('Command options have not been resolved.');
        }

        if ($this->command === 'move') {
            $this->move(
                $this->getOption('sheep_id'),
                $this->getOption('corral_id')
            );
        } elseif ($this->command === 'kill') {
            $this->kill($this->getOption('sheep_id'));
        } else {
            throw new UnknownCommand;
        }
    }

    /**
     * Move sheep to another corral.
     *
     * @param int $sheepId
     * @param int $corralId
     *
     * @return void
     *
     * @throws Exception
     */
    protected function move(int $sheepId, int $corralId): void
    {
        $sheep = Sheep::find($sheepId) ?: throw new Exception('Овечка не найдена.');
        $corral = Sheep::find($corralId) ?: throw new Exception('Загон не найден.');

        if ($sheep->corral_id === $corralId) {
            throw new Exception('Овечка уже находится в этом загоне.');
        }

        $sheep->corral()->associate($corral) ;
        $sheep->save() ?: throw new Exception('Failed to save sheep model.');
    }

    /**
     * Delete sheep.
     *
     * @param int $sheepId
     *
     * @return void
     *
     * @throws Exception
     */
    protected function kill(int $sheepId): void
    {
        $sheep = Sheep::find($sheepId) ?: throw new Exception('Овечка не найдена.');

        $sheep->delete();
    }
}