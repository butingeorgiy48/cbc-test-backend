<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use JetBrains\PhpStorm\Pure;

/**
 * @property int id
 * @property string name
 *
 * @property Collection<Sheep> sheep
 *
 * @mixin Builder
 */
class Corral extends Model
{
    /**
     * @inheritdoc
     *
     * @var bool
     */
    public $timestamps = false;


    # Relations

    /**
     * Return sheep relations.
     *
     * @return HasMany
     */
    public function sheep(): HasMany
    {
        return $this->hasMany(Sheep::class);
    }


    # Other methods

    /**
     * Get amount of sheep.
     *
     * @return int
     */
    #[Pure]
    public function getAmountOfSheep(): int
    {
        return $this->sheep->count();
    }
}
