<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

/**
 * @property int id
 * @property string name
 * @property int corral_id
 *
 * @property Corral corral
 *
 * @mixin Builder
 */
class Sheep extends Model
{
    /**
     * @inheritdoc
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @inheritdoc
     *
     * @var array
     */
    protected $guarded = [];


    # Relations

    /**
     * Return corral relation.
     *
     * @return BelongsTo
     */
    public function corral(): BelongsTo
    {
        return $this->belongsTo(Corral::class);
    }


    # Other methods

    /**
     * Generate many sheep.
     *
     * @param int $amount
     *
     * @return Collection
     */
    public static function generateMany(int $amount = 1): Collection
    {
        $sheep = collect();

        for ($i = 0; $i < $amount; $i++) {
            $sheep->push(new Sheep([
                'name' => 'Овечка'
            ]));
        }

        return $sheep;
    }
}
