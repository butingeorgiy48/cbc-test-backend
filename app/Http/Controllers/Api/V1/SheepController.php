<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Corral;
use App\Models\Sheep;
use App\Services\CorralService\Client as CorralService;
use App\Services\CorralService\Exceptions\CannotRunCommand;
use App\Services\CorralService\Exceptions\InvalidCommand;
use App\Services\CorralService\Exceptions\UnknownCommand;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SheepController extends Controller
{
    /**
     * Load sheep in corrals.
     *
     * @return JsonResponse
     */
    public function load(): JsonResponse
    {
        Corral::all()->each(function (Corral $item) {
            $item->sheep()->saveMany(
                Sheep::generateMany(mt_rand(1, 3))
            );
        });

        return response()->json([
            'changes' => CorralService::representer()->represent()
        ], options: JSON_UNESCAPED_UNICODE);
    }

    /**
     * Refresh sheep in corrals.
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        $refresher = CorralService::refresher();

        $refresher->refresh();

        return response()->json([
            'has_changes' => $refresher->hasChanges(),
            'changes' => $refresher->hasChanges() ? CorralService::representer()->represent() : null
        ], options: JSON_UNESCAPED_UNICODE);
    }

    /**
     * Delete all sheep.
     *
     * @return JsonResponse
     */
    public function clear(): JsonResponse
    {
        Sheep::truncate();

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Execute command.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws CannotRunCommand
     * @throws InvalidCommand
     * @throws UnknownCommand
     */
    public function executeCommand(Request $request): JsonResponse
    {
        CorralService::commandsManager()
            ->resolve($request->get('command'))
            ->run();

        return response()->json([
            'success' => true,
            'changes' => CorralService::representer()->represent()
        ], options: JSON_UNESCAPED_UNICODE);
    }
}
