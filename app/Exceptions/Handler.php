<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (Throwable $e) {
            if ($e instanceof NotFoundHttpException) {
                $status = 404;
            } elseif (property_exists($e, 'status')) {
                $status = $e->status;
            } else {
                $status = 500;
            }

            if ($e instanceof ValidationException) {
                $message = $e->validator->errors()->first() ?? 'The given data was invalid.';
            } elseif ($e instanceof NotFoundHttpException) {
                $message = 'Not Found.';
            } elseif ($e->getMessage()) {
                $message = $e->getMessage();
            } elseif (property_exists($e, 'defaultMessage')) {
                $message = $e->defaultMessage;
            } else {
                $message = 'Internal Server Error.';
            }

            $response = [
                'error' => true,
                'message' => $message
            ];

            if (App::environment('local')) {
                $response['exception'] = get_class($e);
                $response['file'] = $e->getFile();
                $response['line'] = $e->getLine();
                $response['trace'] = $e->getTrace();
            }

            return response()->json($response, $status, options: JSON_UNESCAPED_UNICODE);
        });
    }
}
