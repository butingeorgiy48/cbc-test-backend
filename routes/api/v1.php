<?php

use App\Http\Controllers\Api\V1\SheepController;

Route::post('load', [SheepController::class, 'load']);
Route::post('refresh', [SheepController::class, 'refresh']);
Route::post('clear', [SheepController::class, 'clear']);
Route::post('execute', [SheepController::class, 'executeCommand']);